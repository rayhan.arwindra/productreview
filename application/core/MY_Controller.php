<?php
class MY_Controller extends CI_Controller{
    function __construct(){
        parent::__construct();

        if(isset($_SESSION['logged_in']) && time() - $_SESSION['timestamp'] > 900){
            $this->session->unset_userdata('logged_in');
            $this->session->unset_userdata('username');
            set_cookie('remember', false,time() - 3600);
            redirect('pages/view');
        }else{
            $_SESSION['timestamp'] = time();
        }
    }

}

?>