<?php
class Like_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }

    public function getLikes($reviewId){
        $query = $this->db->query("SELECT count(ratingId) as num FROM `like-rating` WHERE reviewId = ?", array($reviewId));
        return $query->row()->num;
    }

    public function hasLiked($username, $reviewId){
        $this->db->where('username', $username);
        $this->db->where('reviewId', $reviewId);
        $query = $this->db->get('like-rating');

        if ($query->row()){
            return true;
        }

        return false;

    }

    public function addLike($username, $reviewId){
        $data = array(
            'username' => $username,
            'reviewId' => $reviewId,
        );

        $this->db->insert('like-rating', $data);
        return true;
    }
}
?>