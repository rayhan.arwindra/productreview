<div class="card mt-5 shadow">
    <div class="card-header">
        <h3>Items per Category</h3>
    </div>
    <div class="row mb-5">
        <div class="col-md-6">
            <div class="mt-5">
                <canvas id="barChart"></canvas>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mt-1">
                <canvas id="piChart"></canvas>
             </div>
        </div>
    </div>
</div>

<div class="card mt-5 shadow">
    <div class="card-header">
        <h3>Reviews per User</h3>
    </div>
    <div class="row mb-5">
        <div class="col-md-6">
            <div class="mt-3">
                <canvas id="polChart"></canvas>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mt-3">
                <canvas id="donChart"></canvas>
             </div>
        </div>
    </div>
</div>

<div class="card mt-5 shadow">
    <div class="card-header">
        <h3>Reviews Every Month</h3>
    </div>    
    <div class="mt-3 mb-4">
        <canvas id="linChart"></canvas>
    </div>
</div>

<script>

    const random_rgba = () => {
        let round = Math.round, random = Math.random, n = 255;
        return `rgba(${round(random() * n)}, ${round(random() * n)}, ${round(random() * n)}, 0.2)`;
    }


    
    const labelsBar = [
        'Products',
        'Services',
        'Businesses',
    ];
    const dataBar = {
        labels: labelsBar,
        datasets: [{
        label: 'Number of Items',
        backgroundColor: ['rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',],
        borderColor: ['rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',],
        borderWidth: 1,
        data: [<?php echo $products?>,<?php echo $services?>,<?php echo $businesses?> ],
     }]
    };
    const configBar = {
        type: 'bar',
        data: dataBar,
        options: {}
    };
    var barChart = new Chart(
    document.getElementById('barChart'),
    configBar
  );

  const labelsPi = [
        'Products',
        'Services',
        'Businesses',
    ];
    const dataPi = {
        labels: labelsPi,
        datasets: [{
        label: 'Items per Category',
        backgroundColor: ['rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',],
        borderColor: ['rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',],
        borderWidth: 1,
        data: [<?php echo $products?>,<?php echo $services?>,<?php echo $businesses?> ],
     }]
    };
    const configPi = {
        type: 'pie',
        data: dataPi,
        options: {}
    };
    var piChart = new Chart(
    document.getElementById('piChart'),
    configPi
  );


    const reviews =  <?php echo $reviews?>;

    let colors = [];

    for (let i = 0 ; i < reviews.length ; i++){
        colors.push(random_rgba());
    }


    const dataPol = {
    labels: reviews.map(r => r.username),
    datasets: [{
        label: 'My First Dataset',
        data: reviews.map(r => r.review_count),
        backgroundColor: colors,
    }]
    };

    const configPol = {
    type: 'polarArea',
    data: dataPol,
    options: {}
    };

    var chartPol = new Chart(
        document.getElementById('polChart'),
        configPol
    );

 const dataDon = {
  labels: reviews.map(r => r.username)
  ,
  datasets: [{
    label: 'My First Dataset',
    data: reviews.map(r => r.review_count),
    backgroundColor: colors,
    hoverOffset: 4
  }]
};
    const configDon = {
    type: 'doughnut',
    data: dataDon,
    };

    var chartDon = new Chart(
            document.getElementById('donChart'),
            configDon
        );
    const months = ["January", "February", "March","April","May","June","July","August","September","October","November","December"];
    const reviewJson = <?php echo $review_month?>;

    const reviewNum = [];
    reviewNum.push(parseInt(reviewJson[0].count));
    for (let i = 1 ; i < reviewJson.length ; i++){
        reviewNum.push(parseInt(reviewJson[i].count) + reviewNum[i - 1]);
    }

    const reviewMonth = [];
    for (let i = 0 ; i < reviewJson.length ; i++){
        reviewMonth.push(months[parseInt(reviewJson[i].month) - 1]);
    }

    const labelsLine = reviewMonth;
    const dataLine = {
        labels: labelsLine,
        datasets: [{
            label: 'Number of Reviews',
            data: reviewNum,
            fill: false,
            borderColor: 'rgb(75, 192, 192)',
            tension: 0.1
        }],
    };

    const configLine = {
        type: 'line',
        data: dataLine,
    };

    var chartDon = new Chart(
        document.getElementById('linChart'),
        configLine
    );
</script>