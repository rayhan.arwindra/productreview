<div class="card mt-5">
<div class="card-header">
    <h3>Update Password</h3>
</div>
<div class="card-body">
<?php if (validation_errors()) : ?>
        <div class="mt-1 alert alert-danger">
    <?php echo validation_errors();?>
        </div>
    <?php endif?>
    <?php echo form_open('login/forgot_password_update/'.$username);?>
        <div class="form-group mt-4 mb-4">
            <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
        </div>
        <div class="form-group mt-4 mb-4">
          <input type="password" class="form-control" name="passconf" id="exampleInputPassword1" placeholder="Confirm Password">
        </div>
        <button class="btn btn-primary">Submit</button>
    </form>
</div>

</div>