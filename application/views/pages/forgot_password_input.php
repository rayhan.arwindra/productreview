<div class="card mt-5">
<div class="card-header">
    <h3>Find Account</h3>
</div>
<div class="card-body">
   
    <?php echo form_open('login/forgot_password_email');?>
        <p>In order to recover your account, please input your email in the form below</p>
        <div class="form-group mt-4 mb-4">
          <input type="email" class="form-control" name="email" id="exampleInputPassword1" placeholder="Email Address">
        </div>
        <button class="btn btn-primary">Submit</button>
    </form>
</div>

</div>