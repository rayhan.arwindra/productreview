<?php
class Product extends MY_Controller{   
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('product_model');
        $this->load->model('review_model');
        $this->load->model('user_model');
        $this->load->model('like_model');
    }

    public function write_review($itemId = null){
        if($this->session->userdata('logged_in')){
            $data['title'] = 'Review';
            $data['item'] = "";
            $data['default'] = "";
            $data['key'] = $this->config->item('recaptcha_key');
            if ($itemId){
                $data['item'] = $this->product_model->findById($itemId)->name;
                $data['default'] = "readonly";
            }
            $this->load->view('templates/header', $data);
            $this->load->view('pages/write_review', $data);
            $this->load->view('templates/footer');
            
        }else{
            redirect('login/login_view');
        }
    }

    public function add_product(){
        $item_name = $this->input->post('item-name');
        $category = $this->input->post('category');
        if ($this->product_model->newProduct($item_name, $category)){
            redirect('product/write_review');
        }
    }

    public function search(){
        $search = $this->input->get('search-item');
        $data['products'] = $this->product_model->findItems($search);
        $data['title'] = 'Search results for '.$search;
        $data['count'] = count($data['products']);
        $data['query'] = $search;
        $this->load->view('templates/header', $data);
        $this->load->view('pages/search', $data);
        $this->load->view('templates/footer');
    }

    public function fetch_products(){

        $query = '';

        if ($this->input->get('query')){
            $query = $this->input->get('query');
        }
        $data = $this->product_model->fetchItems($query);

        if ($data != null){
            echo json_encode($data);
        }else{
            echo "";
        }
    }

    public function detail_view($item){
        $item = urldecode($item);
        $data['item'] = get_object_vars($this->product_model->findByName($item));
        $data['title'] = 'View '.$item;
        $reviews = $this->review_model->getReviews($data['item']['Id']);

        foreach ($reviews as $i => $review){
            $reviews[$i]['count'] = $this->like_model->getLikes($reviews[$i]['id']);
            if (isset($_SESSION['logged_in'])){
                $reviews[$i]['liked'] = $this->like_model->hasLiked($_SESSION['username'],$reviews[$i]['id']);
            }
        }
        $data['reviews'] = $reviews;
        $data['isFavorite']  = false;
        if ($this->session->userdata('logged_in')){
            $data['isFavorite'] = $this->user_model->isFavorite($_SESSION['username'], $data['item']['Id']);
        }
        $this->load->view('templates/header', $data);
        $this->load->view('pages/item_detail', $data);
        $this->load->view('templates/footer');
    }

    public function add_favorite($item){
        if (!$this->session->userdata('logged_in')){
            redirect('login/login_view');
        }
        $this->product_model->addFavorites($_SESSION['username'], $item);
        $itemObj = $this->product_model->findById($item);
        $itemName = $itemObj->name;
        redirect('product/detail_view/'.$itemName);
    }

    public function add_review(){
        $username = $_SESSION['username'];
        $itemName = $this->input->post('item-name');
        $itemId = $this->product_model->findByName($itemName)->Id;
        $review = $this->input->post('comment');
        $rating = $this->input->post('rating');

        $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
        $userIp=$this->input->ip_address();
        $secret = $this->config->item('recaptcha_secret');
        $url="https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$userIp;

        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, $url); 
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true); 
        $output = curl_exec($request); 
        curl_close($request);      
         
        $status= json_decode($output, true);
        if ($status['success']){
          $this->review_model->makeReview($username, $itemId, $review, $rating);
            redirect('product/detail_view/'.$itemName);
        }else{
            redirect('product/write_review');
        }
    }

    public function add_like($reviewId, $itemName){
        if($this->session->userdata('logged_in')){
            $username = $_SESSION['username'];
            $this->like_model->addLike($username, $reviewId);
            redirect('product/detail_view/'.$itemName);
        }else{
            redirect('login/login_view');
        }
    }

   
}
?>