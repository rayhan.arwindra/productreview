<?php
class Chart extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('product_model');
        $this->load->model('review_model');
        $this->load->model('like_model');
    }

    public function statistics(){
        $data['products'] = $this->product_model->getProducts();
        $data['businesses'] = $this->product_model->getBusinesses();
        $data['services'] = $this->product_model->getServices();
        $data['reviews'] = json_encode($this->review_model->getUserReviews());
        $data['title'] = "Statistic Dashboard";
        $data['review_month'] = json_encode($this->review_model->getNumberOfReviews());
        $this->load->view('templates/header', $data);
        $this->load->view('pages/dashboard', $data);
        $this->load->view('templates/footer');
    }
        


}
?>