<?php
class Email extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('user_model');
    }
    public function send_verification($username){
        $code = bin2hex(random_bytes(9));
        $this->user_model->verificationCode($username, $code);       
        $config  = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mailhub.eait.uq.edu.au',
            'smtp_port' => 25,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => true,
            'starttls' => true,
        );
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from(get_current_user().'@student.uq.edu.au',get_current_user());
        $this->email->to($this->user_model->getEmail($username));
        $this->email->subject('Email verification for '.$username);
        $this->email->message("Hello ".$username.",\nThank you for signing up to this website. Please click the link below to verify your email address and complete your registration.\n https://infs3202-6aff9490.uqcloud.net/productreview/login/verify/".$code);
        $this->email->send();
        redirect('pages/view');
    }

    public function forgot_password($username){
        $code = bin2hex(random_bytes(9));
        $email = $this->user_model->getEmail($username);
        $this->user_model->passwordCode($username, $code);       
        $config  = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mailhub.eait.uq.edu.au',
            'smtp_port' => 25,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => true,
            'starttls' => true,
        );
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from(get_current_user().'@student.uq.edu.au',get_current_user());
        $this->email->to($email);
        $this->email->subject('Reset Password');
        $this->email->message("Hello,\n You requested to change your password. Please click the link below to change your password.\n https://infs3202-6aff9490.uqcloud.net/productreview/login/recover/".$code."\n If you didn't request to change your password, please ignore this email");
        $this->email->send();
        redirect('pages/view');
    }
}
?>